<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "request".
 *
 * @property int $id
 * @property string|null $phone
 * @property string|null $name
 * @property string|null $descript
 * @property string|null $processed
 * @property string|null $create_date
 */
class Request extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'request';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['phone', 'name'], 'required'],
            [['phone', 'name', 'descript', 'processed'], 'string'],
            [['create_date'], 'safe'],
            ['descript', 'match', 'pattern' => '/[A-Za-zА-Яа-яЁё\s]*/'],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'phone' => 'Phone',
            'name' => 'Name',
            'descript' => 'Descript',
            'processed' => 'Processed',
            'create_date' => 'Create Date',
        ];
    }

    public function sendEmail()
    {

        $contact = Company::getInfo();
        if ($this->validate()) {
            Yii::$app->mailer->compose()
                ->setTo($contact['{email}'])
                ->setFrom([$contact['{email}'] => $contact['{name}']])
                ->setSubject('Позвони мне позвони')
                ->setTextBody(
                    'Номер заявки:' .$this->id . '
' . $this->name .'-' . $this->phone . '
Дополнения:
' . $this->descript . '
Получена заявка:'. $this->create_date)
                ->send();

            return true;
        }
        return false;
    }
}
