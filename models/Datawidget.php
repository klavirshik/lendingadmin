<?php

namespace app\models;



use Yii;

/**
 * This is the model class for table "datawidget".
 *
 * @property int $id
 * @property int|null $idwidget
 * @property string|null $title
 * @property string|null $data
 *
 * @property Listwidget $idwidget0
 */
class Datawidget extends \yii\db\ActiveRecord
{
    const DEFAULT_NAME = 'new';
    const DEFAULT_VALUE = '';
    public $src;
    public $description;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'datawidget';
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
           if(($this->src != null)or($this->description != null)) $this->data = $this->src . '|' . $this->description;

            return true;
        }
        return false;
    }

    public function afterFind()
    {
        if(strpos($this->data,'|') != false)
            list($this->src, $this->description) = explode('|',$this->data);

    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idwidget'], 'integer'],
            [['title', 'data', 'src' , 'description'], 'string'],
            [['idwidget'], 'exist', 'skipOnError' => true, 'targetClass' => Listwidget::className(), 'targetAttribute' => ['idwidget' => 'id']],
        ];
    }

    public function addOne($userId)
    {
        $this->idwidget = $userId;
        $this->title = self::DEFAULT_NAME;
        $this->data = self::DEFAULT_VALUE;
    }


    /**
     * {@inheritdoc}
     *
     *

     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idwidget' => 'Idwidget',
            'title' => 'Title',
            'data' => 'Data',
            'src' => 'Путь',
            'description' => 'Описание',
        ];
    }

    /**
     * Gets query for [[Idwidget0]].
     *
     * @return \yii\db\ActiveQuery
     */

    public function getWidget()
    {
        return $this->hasOne(Listwidget::className(), ['id' => 'idwidget']);
    }
}
