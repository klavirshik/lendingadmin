<?php

namespace app\models;

use phpDocumentor\Reflection\Types\Array_;
use Yii;
use yii\base\Model;
use app\models\Listwidget;

class ListMenu extends Model
{
    public $label;
    public $url;
    public $icon;



    public function rules()
    {
        return [
            [['label', 'url'], 'required'],
            ['icon'],


        ];
    }

    private static function getIcon($Icon){
        if($Icon === 'Y'){
            return 'circle-o';
        }
        return 'eye-slash';

    }

    public static function getMenu()
    {
        $item = [

            ['label' => 'Инструменты', 'options' => ['class' => 'header']],
            ['label' => 'Заявка на звонок', 'icon' => 'fax', 'url' => ['/admin/index']],
            ['label' => 'Администрирование', 'icon' => 'group', 'url' => ['/admin/userlist']],
            ['label' => 'Порядок виджетов', 'icon' => 'bars', 'url' => ['/admin/position']],
            ['label' => 'Данные компании', 'icon' => 'building-o', 'url' => ['/company/index']],
            ['label' => 'Login', 'url' => ['admin/login'], 'visible' => Yii::$app->user->isGuest],
            ['label' => 'Виджеты', 'options' => ['class' => 'header']]
        ];


        $menu_items = Listwidget::find()->orderBy('position')->all();
            foreach ($menu_items as $menu_item){
                $item[] = [
                    'label' => $menu_item['label'],
                    'url' => '/admin/update?id=' .  $menu_item['id'],
                    'icon' => self::getIcon($menu_item['type']),//eye-slash  circle-o
                ];
            }
            $item[] =['label' => 'Добавить виджет', 'icon' => 'plus-square-o', 'url' => ['/admin/new']];
            return $item;
    }
}
