<?php

namespace app\models;

use Yii;
use yii\base\Model;
use app\models\User;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class NewUserForm extends Model
{
    public $username;
    public $password;
    public $password_repeat;
    private $_user = false;


    public function rules()
    {
        return [
            [['username', 'password','password_repeat'], 'required'],
            ['username', 'validateUserName'],
            ['password', 'compare', 'compareAttribute' => 'password_repeat'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'username' => 'Логин',
            'password' => 'Введите новый пароль',
            'password_repeat' => 'Повторите пароль',
        ];
    }


    public function validateUserName($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $this->getUserName();

            if (!$this->_user === false) {
                $this->addError($attribute, 'Такой логин уже существует.');
            }
        }
    }
    public function save(){

        if ($this->validate()) {
            $user = new User();
            $user->username  = $this->username;
            $user->setPassword($this->password);
            $user->save();
            return true;
        }
        return false;
    }
    /**
     * Logs in a user using the provided username and password.
     * @return bool whether the user is logged in successfully
     */


    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public function getUserName()
    {
        if ($this->_user === false) {
            $this->_user = User::findByUsername($this->username);
        }

        return $this->_user;
    }
}
