<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "listwidget".
 *
 * @property int $id
 * @property int|null $position
 * @property string|null $data
 * @property string|null $type
 * @property string|null $idname
 * @property string|null $tampl
 * @property string|null $label
 *
 * @property Datawidget[] $datawidgets
 */
class Listwidget extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'listwidget';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['position'], 'integer'],
            [['data', 'type', 'idname', 'tampl', 'label'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'position' => 'Порядок',
            'data' => 'Data',
            'type' => 'Видимость',
            'idname' => 'Уникальный индификатор',
            'tampl' => 'Тип',
            'label' => 'Название',

        ];
    }

    /**
     * Gets query for [[Datawidgets]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDatawidgets()
    {
        return $this->hasMany(Datawidget::className(), ['idwidget' => 'id']);
    }
}
