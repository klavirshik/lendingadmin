<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class PasswordResetForm extends Model
{
    public $user;
    public $password;
    public $password_repeat;
    public $Aythkey;

    public $_user = false;
    /**
     * @return array the validation rules.
     */
    public function __construct($Aythkey,$config = [])
    {
        $this->_user = $this->getUser($Aythkey);

        parent::__construct($config);
    }

    public function resetPassword()
    {

        $this->_user->setPassword($this->password);
        $this->_user->generateAuthkey();
        $this->_user->save();
        return true;
    }

    public function rules()
    {
        return [
            [['password', 'password_repeat'], 'required'],
            ['password', 'compare', 'compareAttribute' => 'password_repeat'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'password' => 'Введите новый пароль',
            'password_repeat' => 'Повторите пароль',
        ];
    }


    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public function getUser($Aythkey)
    {
        if ($this->_user === false) {
            $this->_user = User::findByAythkey($Aythkey);
        }

        return $this->_user;
    }
}
