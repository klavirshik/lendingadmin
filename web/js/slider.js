$(document).ready(function () {





})

class SliderWd{
    slideInterval = 1000;
    translateWidth = 0;
    slideNow = 1;
    slideCount;
    navBtnId = 0;
    switchInterval;
    name_widget;

    constructor(name_widget, slideInterval) {
        this.slideCount = $('#slidewrapper'+ name_widget).children().length;
        this.name_widget = name_widget;
        this.slideInterval = slideInterval;
        this.addNav('slide-nav-btn');
        this.setAutoSlide();
        this.addButton();
        this.redrow(this.slideNow);
        this.switchInterval;


    }

    redrow() {
        $('.slide-nav-btn'+ this.name_widget).each(function(indxx, element){
            $(element).stop(true, true);
            if(indxx == this.slideNow - 1){
                $(element).animate( {width: "11px",height: "11px" } , 1000 );
            }else {
                $(element).animate( { width: "8px",height: "8px" } , 1000 );
            }
        }.bind(this));
    }

    addNav(nameBtn){
        let item = $('.'+nameBtn+ this.name_widget);
        for (let i = 0; i < item.length; i++){
            item.eq(i).click(function () {
                let navBtnId = i;
                if (navBtnId + 1 != this.slideNow) {
                    this.translateWidth = -$('#viewport' + this.name_widget).width() * (navBtnId);
                    this.moveSlide(this.translateWidth);
                    this.slideNow = navBtnId + 1;
                    this.redrow();
                }
            }.bind(this));
        }
    }
    addButton() {
        // добавляем кнопки
        $('#next-btn'+ this.name_widget).click(function() {
            this.nextSlide();
        }.bind(this));

        $('#prev-btn'+ this.name_widget).click(function() {
            this.prevSlide();
        }.bind(this));
    }
    setAutoSlide() {
        // добавляем авто прокрутку
        this.switchInterval = setInterval($.proxy(this.nextSlide, this), this.slideInterval);
        $('#viewport'+ this.name_widget).hover(function(){//автопрокрутка
            clearInterval(this.switchInterval);
        }.bind(this),function() {
            this.switchInterval = setInterval($.proxy(this.nextSlide, this), this.slideInterval);
        }.bind(this));

    }
    moveSlide(translateWidth){
        $('#slidewrapper' + this.name_widget).css({
            'transform': 'translate(' + translateWidth + 'px, 0)',
            '-webkit-transform': 'translate(' + translateWidth + 'px, 0)',
            '-ms-transform': 'translate(' + translateWidth + 'px, 0)',
        });
    }


    nextSlide() {
        if (this.slideNow == this.slideCount || this.slideNow <= 0 || this.slideNow > this.slideCount) {
            $('#slidewrapper' + this.name_widget).css('transform', 'translate(0, 0)');
            this.slideNow = 1;
        } else {
            this.translateWidth = -$('#viewport' + this.name_widget).width() * (this.slideNow);
            this.moveSlide(this.translateWidth);
            this.slideNow++;
        }
        this.redrow();
    }
    prevSlide() {
        if (this.slideNow == 1 || this.slideNow <= 0 || this.slideNow > this.slideCount) {
            this.translateWidth = -$('#viewport' + this.name_widget).width()* (this.slideCount - 1);
            this.moveSlide(this.translateWidth);
                this.slideNow = this.slideCount;
        } else {
            this.translateWidth = -$('#viewport' + this.name_widget).width() * (this.slideNow - 2);
            this.moveSlide(this.translateWidth);
            this.slideNow--;
        }
        this.redrow();
    }

}

class SliderMain extends SliderWd{
    constructor(...args) {
        super(...args);
        super.addNav('slide-nav-tx');
    }

    redrow() {
        super.redrow();
        $('.slide-nav-tx'+ this.name_widget).each(function(indx, element){
            $(element).stop(true, true);
            if(indx == this.slideNow - 1){
                $(element).animate( { fontSize:"1.3em" } , 1000 );
            }else {
                $(element).animate( { fontSize:"1em" } , 1000 );
            }
        }.bind(this));
    }
}