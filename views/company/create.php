<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Company */

$this->title = 'Данные компании';

$this->params['breadcrumbs'][] = $this->title;
?>
<div class="company-create">



    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
