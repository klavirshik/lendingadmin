<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Company */

$this->title = 'Данные компании: ' . $model->name;

$this->params['breadcrumbs'][] = 'Изменить';
?>
<div class="company-update">



    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
