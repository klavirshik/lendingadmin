<?php

use app\models\Company;
use yii\helpers\Html;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $idwidget app\models\Listwidget */
/* @var $model app\models\Listwidget */
/* @var $datawidget app\models\Datawidget */

?>
<div class="content">
    <div id="viewport<?= $model->idname ?>" class="viewport1">
        <ul id="slidewrapper<?= $model->idname ?>" class="slidewrapper1">
            <?php foreach ($idwidget as $id => $datawidget) { ?>
                <li class="slide1">
                    <div class="about_img">
                        <img src="<?= $datawidget->src ?>" alt="1" class="slide-img">
                    </div>
                    <div class="about_list">
                        <h1>Наши проекты</h1>
                        <h2><?= $datawidget->title ?></h2>
                        <p> <?= $datawidget->description ?></p>
                    </div>
                </li>
            <?php } ?>
        </ul>

        <div id="prev-next-btns<?= $model->idname ?>" class="prev-next-btns1">
            <div id="prev-btn<?= $model->idname ?>" class="prev-btn1"></div>
            <div id="next-btn<?= $model->idname ?>" class="next-btn1"></div>
        </div>

        <ul id="nav-btns<?= $model->idname ?>" class="nav-btns1">
            <?php foreach ($idwidget as $id => $datawidget) { ?>
                <li class="slide-nav-btn<?= $model->idname ?> slide-nav-btn1"></li>
            <?php } ?>
        </ul>

    </div>
</div>
<?php $this->registerJs(
    $model->idname."AbotWdg = new SliderWd('" . $model->idname . "', 4000);",
    View::POS_LOAD);
?>
