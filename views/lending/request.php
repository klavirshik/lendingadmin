<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Request;

/* @var $this yii\web\View */
/* @var $model app\models\Listwidget */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="popup">
    <div class="close_modal">x</div>
    <?php

    $form = ActiveForm::begin([
            'action' => '',
            'options' => [
            'class' => 'fofm'
        ],
            'id' => 'UpdateForm']
    );
    $model = new Request();
    ?>
    <h5>Оставить заявку</h5>
    <?=  $form->field($model, 'name')->textInput(['placeholder'=>'Имя'])->label(false) ?>
    <?=  $form->field($model, 'phone')->textInput(['placeholder'=>'Телефон' ,
        'pattern' => '^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$',
        'required' => '',
        'type' => 'tel',] )->label(false) ?>
    <?=  $form->field($model, 'descript')->textarea(['placeholder'=>'Дополнительные пожелани',
        'rows'=>'10', ])->label(false) ?>


    <label><?= $this->params['{rule_call}'] ?></label>
    <input type="hidden" name="valTrFal" class="valTrFal" value="valTrFal_disabled">

    <?= Html::submitButton('Отправить', ['class' => 'button']) ?>
    <?php ActiveForm::end();?>

</div>
<div class="popup2">
    <div class="close_modal">x</div>
    <div class="window">
        <div class="insText">
            <h5>запрос отправлен</h5>
            <p><strong>Ваш запрос отправлен.</strong>Наш менеджер свяжется с вами в ближайшее время!</p>
            <hr>

        </div>
    </div>
</div>