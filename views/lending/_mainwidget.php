<?php

use app\models\Company;
use yii\helpers\Html;
use yii\web\View;


/* @var $this yii\web\View */
/* @var $idwidget app\models\Listwidget */
/* @var $model app\models\Listwidget */

?>
<div class="content">
    <div id="viewport<?= $model->idname ?>" class="viewport_main">
        <ul id="slidewrapper<?= $model->idname ?>" class="slidewrapper_main">
            <?php foreach ($idwidget as $id => $datawidget) { ?>
            <li class="slide_main slide<?= $model->idname ?>"><img src="<?= $datawidget->data ?>" alt="1" class="slide-img<?= $model->idname ?> slide-img_main"></li>
            <?php } ?>

        </ul>

        <div id="border_right<?= $model->idname ?>" class="border_right_main"></div>
        <div id="border_left<?= $model->idname ?>" class="border_left_main"></div>

        <div id="prev-next-btns<?= $model->idname ?>" class="prev-next-btns_main">
            <div id="prev-btn<?= $model->idname ?>" class="prev-btn_main"></div>
            <div id="next-btn<?= $model->idname ?>" class="next-btn_main"></div>
        </div>

        <ul id="nav-btns<?= $model->idname ?>" class="nav-btns_main">
            <?php foreach ($idwidget as $id => $datawidget) { ?>
            <li class="slide-nav-btn<?= $model->idname ?> slide-nav-btn_main"></li>
            <?php } ?>
        </ul>

        <ul id="nav-text<?= $model->idname ?>" class="nav-text_main">
            <?php foreach ($idwidget as $id => $datawidget) { ?>
            <li class="slide-nav-tx<?= $model->idname ?> slide-nav-tx_main"><?= $datawidget->title ?></li>
            <?php } ?>
        </ul>

    </div>
</div>
<?php $this->registerJs(
    $model->idname."ImgWdg = new SliderMain('" . $model->idname . "', 4000);",
    View::POS_LOAD);
?>