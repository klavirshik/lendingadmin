<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;


/* @var $form yii\widgets\ActiveForm */
/* @var $this yii\web\View */
/* @var $model app\models\Listwidget */

$this->title = 'Добавить виджет';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
        <div class="box-tools pull-right">

        </div>
        <!-- /.box-tools -->
    </div>
    <!-- /.box-header -->
    <div class="box-body">

        <?php $form = ActiveForm::begin(); ?>


        <?= $form->field($model, 'label')->textInput() ?>

        <?= $form->field($model, 'tampl')->dropDownList([
            '_mainwidget' => 'Виджет с картинками',
            '_sale' => 'Текстовый блок',
            '_listwidget' =>'Виджет с текстом',
            '_img'=> 'Картинка',
            '_map'=> 'Карта яндекс',
        ], ['prompt' => '']) ?>

        <?= $form->field($model, 'idname')->textInput() ?>




        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>


        <?php ActiveForm::end(); ?>
    </div>

</div>




