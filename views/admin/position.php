<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\Pjax;
use yii\web\View;
use yii\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $searchModel app\models\listWidget */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Очередность виджетов';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="col-8">





        <div class="box  box-primary">
            <div class="box-header with-border">
                <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
                <div class="box-tools pull-right">
                </div>
            </div>
            <div class="box-body container ">
                <h4>
    <?php
    $form = ActiveForm::begin( ['action' => Url::toRoute(['admin/updatepos']),]);
    foreach ($model as $key => $item){?>
        <div  class="drag " id= '<?= Html::encode($key) ?>' style="border-bottom: 1px solid #0b3e6f; padding: 10px; width: 70%;"> <?= Html::encode($item->label) ?></div>

    <?php } ?>
                </h4>
            </div>
        </div>
    <div id="pos-list">
    <?php foreach ($model as $key => $item){?>
        <?= $form->field($item, '[' . $key . ']position')->hiddenInput()->label(false) ?>
        <?= $form->field($item, '[' . $key . ']id')->hiddenInput()->label(false) ?>
    <?php } ?>
    </div>
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success',
                                                'id' => 'pos',
                                                'name' => 'yii'
                                                                            ]) ?>

    <?php $form = ActiveForm::end();

    ?>


</div>
