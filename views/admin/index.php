<?php

use yii\helpers\Html;
use yii\helpers\Url;
use app\models\Request;

/* @var $this yii\web\View */
/* @var $model app\models\Request */

$this->title = 'Админка';
?><div class="box">
    <div class="box-header">
        <h3 class="box-title">Таблица заявок</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body no-padding">
        <table class="table table-striped">
            <tbody><tr>
                <th style="width: 10px">#</th>
                <th>Имя</th>
                <th>Номер телефона</th>
                <th>Описание</th>
                <th>Время создания</th>
                <th style="width: 30px">Статус</th>
                <th style="width: 180px"></th>
            </tr>
   <?php foreach ($model as $key => $item) { ?>
       <tr>
           <td><?= Html::encode($key)?></td>
           <td><?= Html::encode($item->name)?></td>
           <td><?= Html::encode($item->phone)?></td>
           <td><?= Html::encode($item->descript)?></td>
           <td><?= Html::encode($item->create_date)?></td>
           <td>
           <?php if($item->processed){?>
               <span class="badge ">ok</span></td>
           <?php }else{ ?>
               <span class="badge bg-red">NEW</span></td>
           <?php }; ?>
           <td>  <?= Html::a('Закрыть', Url::toRoute(['admin/rereq', 'id' => $item->id]), [
               'class' => 'btn btn-info',
           ]) ?>
           <?= Html::a('Удалить', Url::toRoute(['admin/delreq', 'id' => $item->id]), [
               'class' => 'btn btn-warning',
           ]) ?></td>
       </tr>

   <?php }  ?>



            </tbody></table>
    </div>
    <!-- /.box-body -->
</div>