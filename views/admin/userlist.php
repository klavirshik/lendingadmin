<?php

use yii\helpers\Html;
use yii\helpers\Url;
use app\models\Request;

/* @var $this yii\web\View */
/* @var $model app\models\Request */

$this->title = '';
?><div class="box">
    <div class="box-header">
        <h3 class="box-title">Пользователи</h3> <?= Html::a('Новый', Url::toRoute(['admin/newuser']), [
            'class' => 'btn btn-success',
        ]) ?>
    </div>

    <!-- /.box-header -->
    <div class="box-body no-padding">
        <table class="table table-striped">
            <tbody><tr>
                <th style="width: 10px">#</th>
                <th>Логин</th>
                <th>Создан</th>
                <th>Изменен</th>


                <th style="width: 240px"></th>
            </tr>
   <?php foreach ($model as $key => $item) { ?>
       <tr>
           <td><?= Html::encode($key)?></td>
           <td><?= Html::encode($item->username)?></td>

           <td><?= Html::encode(Yii::$app->formatter->asDatetime($item->created_at)) ?></td>
           <td><?= Html::encode(Yii::$app->formatter->asDatetime($item->updated_at))?></td>

           <td>  <?= Html::a('Сменить пароль', Url::toRoute(['admin/passreset', 'token' => $item->authKey]), [
               'class' => 'btn btn-info',
           ]) ?>
           <?= Html::a('Удалить', Url::toRoute(['admin/deluser', 'token' => $item->authKey]), [
               'class' => 'btn btn-danger',
           ]) ?></td>
       </tr>

   <?php }  ?>



            </tbody></table>
    </div>

    <!-- /.box-body -->
</div>