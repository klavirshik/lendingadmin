<?php

use yii\widgets\Pjax;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use mihaildev\elfinder\InputFile;
use mihaildev\elfinder\ElFinder;
use yii\web\JsExpression;
use  app\models\Listwidget;

/* @var $this yii\web\View */
/* @var $model app\models\Listwidget */
/* @var $form yii\widgets\ActiveForm */

?>








<?php Pjax::begin(['enablePushState' => false]);?>

<?php $form = ActiveForm::begin([
    'action' => Url::toRoute(['admin/updatedata', 'userId' => $model->id]),
    'options' => [
        'data-pjax' => '1'
    ],
    'id' => 'UpdateForm'
]); ?>



    <?= $form->field($model, 'label')->textInput() ?>

    <?= $form->field($model, 'type')->dropDownList([ 'Y' => 'Y', 'N' => 'N', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'idname')->textInput() ?>






<?php
    foreach ($model->datawidgets as $id => $datawidget) {?>
    <div class="col-md-4">
            <div class="box box-solid box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Слайд <?= $id + 1 ?></h3>
                        <div class="box-tools pull-right">
                        </div>
                 </div>
                 <div class="box-body">
        <?=  $form->field($datawidget, '[' . $id . ']title')->textInput() ?>

                     <img src="<?= $datawidget->data ?>" width="100%">

        <?= $form->field($datawidget, '[' . $id . ']data')->widget(InputFile::className(), [
            'language'      => 'ru',
            'controller'    => 'elfinder', // вставляем название контроллера, по умолчанию равен elfinder

            'filter'        => 'image',    // фильтр файлов, можно задать массив фильтров https://github.com/Studio-42/elFinder/wiki/Client-configuration-options#wiki-onlyMimes
            'template'      => '<div class="input-group">{input}<span class="input-group-btn">{button}</span></div>',
            'options'       => ['class' => 'form-control'],
            'buttonOptions' => ['class' => 'btn btn-success'],
            'multiple'      => false       // возможность выбора нескольких файлов
        ]);?>

        <?= Html::a('Удалить', Url::toRoute(['admin/deleteall', 'id' => $datawidget->id]), [
            'class' => 'btn btn-danger',
        ]) ?>


                 </div>
            </div>
    </div>

    <?php
    }

?>
<div class="col-md-12">
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a('Добавить', Url::toRoute(['admin/createdata', 'userId' => $model->id]), [
            'class' => 'btn btn-success',
        ]) ?>
    </div>
</div>
<?php ActiveForm::end(); ?>

<?php Pjax::end(); ?>


