<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use mihaildev\elfinder\InputFile;
use mihaildev\elfinder\ElFinder;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model app\models\Listwidget */
/* @var $form yii\widgets\ActiveForm */
$this->title = 'Новый пользователь';
?>



    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'username')->textInput()?>
    <?= $form->field($model, 'password')->passwordInput()?>
    <?= $form->field($model, 'password_repeat')->passwordInput()?>

        <?= Html::submitButton('Cохранить', ['class' => 'btn btn-success']) ?>



    <?php ActiveForm::end(); ?>

