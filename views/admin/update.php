<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Listwidget */

$this->title = 'Редактирование виджета: id' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Виджет id' . $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>

<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Редактор виджета</h3>
        <div class="box-tools pull-right">

        </div>
        <!-- /.box-tools -->
    </div>
    <!-- /.box-header -->
    <div class="box-body">






    <?= $this->render('_form' . $model->tampl, [
        'model' => $model,
    ]) ?>

</div>
</div>
