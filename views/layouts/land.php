<?php

/* @var $this \yii\web\View */
/* @var $content string */
/* @var $info app\models\Company */
/* @var $form yii\widgets\ActiveForm */


use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\models\Request;
use app\assets\MyAppAsset;

$this->registerLinkTag(['rel' => 'icon', 'type' => 'image/ico', 'href' => Url::to(['/favicon.ico'])]);
MyAppAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
    <div class="page">
        <div class="head">
            <div class="head_logo">
                <img src="/css/img/logo.png">
            </div>

            <div class="head_contact"> <?= Html::encode($this->params['{phone}']) ?>
                <br><?= Html::encode($this->params['{email}'])?>
                <br> <?= Html::encode($this->params['{address}'])?>
            </div>
        </div>
        <?= $content ?>

        <div class="foot">
            <div class="foot_logo">
                <img src="/css/img/foot_logo.png">
                <a href="mailto:klavirshik@yandex.ru"><div class="autor">Powered by Klavirshik</div></a>
            </div>

            <div class="foot_contact"> <?= Html::encode($this->params['{phone}']) ?>
                <br><?= Html::encode($this->params['{email}'])?>
                <br> <?= Html::encode($this->params['{address}'])?>
                <br>© 2020 Partner38
                <br>
            </div>
        </div>
    </div>

<div class="overlay"></div>




<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>