<?php

use app\models\ListMenu;

$items = ListMenu::getMenu();
?>
<aside class="main-sidebar">

    <section class="sidebar">



        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' =>  $items,


            ]
        ) ?>

    </section>/

</aside>
