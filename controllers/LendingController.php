<?php

namespace app\controllers;

use yii\web\Controller;
use app\models\Company;
use app\models\Listwidget;
use app\models\Request;

use app\models\ListwidgetSearch;
use Yii;


class LendingController extends \yii\web\Controller
{
    public $layout = 'land';

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],

        ];
    }

    public function actionAddcall()
    {


        $model = new Request();
        $model->create_date = date("Y-m-d H:i:s");
        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            $model->sendEmail();
            return  'ok';
        }else{
            return  false;
        }






    }

    public function actionIndex()
    {



        $this->view->params = Company::getInfo();
        $modules = Listwidget::find()->orderBy('position')->all();

        return $this->render('index', [ 'modules' => $modules,
                                              'model' => null]);
    }

}
